# Sprint Project 1 

### My APP _Delilah Resto_

#### _Machado Daiana_


El sprint project 1, indicaba la creación de una API para la gestión de todos los pedidos del restaurante _Delilah Resto_

Se realizaron las operaciones CRUD de las siguientes entidades:
- Usuarios
- Productos
- Pedidos
- Métodos de Pago


### <b>Instalación </b>

Las siguientes dependencias son las utilizadas dentro de la API de Delilah Restó

- [Node](https://nodejs.org/) 
- [Nodemon](https://www.npmjs.com/package/nodemon) 
- [Express](https://www.npmjs.com/package/express)
- [JS-YAML](https://www.npmjs.com/package/js-yaml)
- [Swagger](https://www.npmjs.com/package/swagger-ui-express)

Las librerias mencionadas anteriormente deben instalarse desde el repositorio. 
Los comandos sugeridos para la instalacioón de las dependencias y librerias son los siguientes:

```sh
npm init

npm i node nodemon

npm i express swagger-ui-express js-yaml
```
Luego y para poder dar inicio el servidor, debe utilizar el siguiente comando:
```sh
npx nodemon API/servidor.js
```
Una vez iniciado, el servidor se encuentra cargando en el puerto 3000. 

### <b>Testeo </b>

La API de Delilah Restó se puede probar dentro de la interfaz de Swagger, se va a poder acceder desde el siguiente link:

[API - Delilah Restó // Swagger](http://localhost:3000/api-docs/)

Las rutas estan agrupadas según las entidades anteriormente mencionadas. Para facilitar el testeo, en cada una de ellas existen cargados usuarios, productos, pedidos y métodos de pago.

### _Usuarios_

Los <b>usuarios</b> tienen diferentes parámetros que le permiten acceder o no a diferentes rutas. Los usuarios pueden registrarse e iniciar sesión, eso les va a permitir realizar gran parte de las operaciones. 
- Pueden crear un nuevo usuario
- Iniciar sesión
- Ver su historial de pedidos

Los <b>administradores registrados</b> tendrán acceso a otros operaciones más específicas. 
- Ver todos los usuarios con sus datos
- Eliminar un usuario
- Asignarle la posibilidad a un usuario de ser administrador 


Existen usuarios que facilitan el testeo, en la interfaz de Swagger. Para el acceso de la mayoría de las rutas se pide el ID por el headers, y según sea su condición (si es un usuario que inició sesión o no, si es un administrador que inició sesión o no), le permitirá (o no), continuar con las diferentes operaciones. 

<b>Administrador registrado</b>
- idUsuario: 1
- usuario: dai_machado
- mail: machadodaianag@gmail.com
- password: 123456

<b>Administrador NO registrado</b>
- idUsuario: 2
- usuario: juan_machado
- mail: machadojuan@gmail.com
- password: 123456

<b>Usuario registrado</b>
- idUsuario: 3
- usuario: trini
- mail: trini@gmail.com
- password: 123456

<b>Usuario NO registrado</b>
- idUsuario: 4
- usuario: pedro
- mail: pedro@gmail.com
- password: 123456



### _Productos_

Existen <b>productos</b> predeterminados para poder facilitar el testeo, sus ID van del 1 al 6, y suelen ser pedidos mediante el _path_. Para los productos se realizaron las operaciones CRUD completas.
<p>Cabe destacar, que todas estas operaciones son facultades exclusivas del administrador debidamente registrado, cualquier usuario tiene negado el acceso a esas rutas. </p>

- Crea nuevos productos.
- Modifica productos existentes.
- Elimina productos existentes.

Excepto esta operación, la cual pueden acceder cualquier usuario, registrado o no: 
- Muestra todos los productos.
### _Pedidos_

Existen 3 <b>pedidos</b> predeterminados, con sus correspondientes ID. Existen operaciones que pueden realizar aquellos usuarios registrados:
- Iniciar un pedido
- Agregar productos a un pedido
- Sacar productos del pedido
- Pagar el pedido

La posibilidad de agregar o sacar productos del pedido, es decir, modificar su pedido, se hace con la condición que no hayan pagado el mismo. Al pagar, deben elegir un método de pago (activo), podrán poner un dirección de envío del pedido (de no hacerlo, se enviará a la que han puesto en su registro), y por último, se establece que el pedido se encuentra en estado "Pendiente" y "Cerrado" (no se permiten más modificaciones). Automáticamente esos productos comprados, migran al historial de pedidos de cada usuario.


Las operaciones que corresponden únicamente a los administradores registrados son:
- Poder ver todos los pedidos.
- Modificar el estado de los pedidos.
- Eliminar pedidos existentes.

Cuando se modifica el estado del pedido, el id del pedido y el estado, se establece que si el estado que se pone es "Entregado", posteriormente no se pueden generar cambios a ese estado.

### _Métodos de Pago_

Los <b>métodos de pago</b> predeterminados para poder facilitar el testeo, son 3 (con sus corrrespondientes ID), suelen ser pedidos mediante el _path_ para las operaciones CRUD específicas. Cuando se vincula con pagos, el ID es requerido en el body.
<p>Cabe destacar, que al igual que los productos todas estas operaciones son facultades exclusivas del administrador debidamente registrado, cualquier usuario tiene negado el acceso a esas rutas. </p>
<p>La operación <B> PUT </B> permite al administrador cambiar el estado del método de pago, siendo este Activo o Inactivo. Que al mismo tiempo se encuentra vinculado con la posibilidad de que el usuario pueda o no pagar con ese método de pago dependiendo su estado. </p>



### Daiana Machado
<p> machadodaianag@gmail.com </p>
