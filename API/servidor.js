const express = require('express');
const helmet = require('helmet');
const { iniciarSesion } = require('./Funciones/usuarios');
const { autenticacion } = require('./Middleware/delservidor');
const { cargarSwaggerInfo } = require('./Middleware/documentation');
const { routersMetodosPago } = require('./Routers/metodosPago');
const { routersPedidos } = require('./Routers/pedidos');
const { routersProductos } = require('./Routers/productos');
const { routersUsuarios } = require('./Routers/usuarios');
require("dotenv").config();
const servidor = express();

PORT = 3000

servidor.use (helmet());
servidor.use (express.json ());

servidor.post ('/iniciarsesion', autenticacion, iniciarSesion);

servidor.use(routersUsuarios());
servidor.use(routersPedidos());
servidor.use(routersProductos());
servidor.use(routersMetodosPago());



cargarSwaggerInfo (servidor);



servidor.listen(PORT, () => console.log(`Cargando en el puerto ${PORT}`));