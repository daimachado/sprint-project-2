
const yaml = require('js-yaml');
const fs = require('fs');
const swaggerUi = require('swagger-ui-express');

//Ruta para interfaz de documentación

function cargarSwaggerInfo(servidor) {
    try {
        const documentation = yaml.load(fs.readFileSync('./API/Middleware/spec.yml', 'utf8'));
        servidor.use('/api-docs', swaggerUi.serve, swaggerUi.setup(documentation));
        //console.log (documentation)
    } catch (e) {
        console.log(e);
        //console.log('Esta pasando por el catch')
    }
};


module.exports = {

    cargarSwaggerInfo
};
