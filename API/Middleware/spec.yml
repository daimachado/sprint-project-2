openapi: 3.0.0
info:
  version: 3.0.1
  title: Delilah Resto Servidor
  description: Es el servidor de Delilah Resto.
license:
  name: Apache 2.0
  url: https://www.apache.org/licenses/LICENSE-2.0.html
servers:
  - url: http://localhost:3000/
    description: Development server
tags:
  - name: Usuarios
    description: Operaciones sobre los usuarios
  - name: Productos
    description: Operaciones sobre los productos
  - name: Pedidos
    description: Operaciones sobre los pedidos
  - name: Métodos de pago
    description: Operaciones sobre los métodos de pago
paths:
  /iniciarsesion:
    post:
      tags:
        - Usuarios
      summary: Iniciar sesión.
      description: Permite al usuario registrado iniciar sesión.
      requestBody:
        required: true
        content: 
          application/json:
            schema:
              type: object 
              properties:
                usuario:
                  type: string
                  example: trini
                password:
                  type: string
                  example: 123456
      responses:
        200:
          description: Se ha registrado exitosamente.
        404:
          description: No existe o la contraseña es incorrecta.
  /usuarios:
    get:
      tags:
        - Usuarios
      summary: Muestra todos los usuarios.
      description: Permite al administrador ver todos los usuarios.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
      responses:
        200:
          description: Muestra al administrador toda la información de los usuarios.
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
    post:
      tags:
        - Usuarios
      summary: Crea un nuevo usuario.
      description: Se crean nuevos usuarios, obtiene la información y verifica que el correo electrónico no esté duplicado.
      requestBody:
        required: true
        content: 
          application/json:
            schema:
              type: object 
              properties:
                usuario:
                  type: string
                  example: dai_machado
                nombreApellido:
                  type: string
                  example: Daiana Machado
                mail:
                  type: mail
                  example: m@gmail.com
                password:
                  type: string
                  example: "123456"
                telefono:
                  type: integer
                  example: 290123456
                agendaDirecciones:
                  type: object
                  properties: 
                    direccion:
                      type: string
                      example: P sherman 
                    detalle:
                      type: string
                      example: Nemo
      responses:
        200:
          description: El usuario ha sido registrado con éxito.
        401:
          description: Para registrarse debe completar todos los campos.
        404:
          description: Ya ha sido registrado. ecupere su cuenta o intente con otro correo electrónico.
  /usuarios/{id}:
    get:
      tags:
        - Usuarios
      summary: Muestra historial de pedidos.
      description: Muestra a los usuarios registrados su historial de pedidos.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          required: true
          description: UsuarioID de quien va a realizar la operación
          schema:
            type: string
        - name: id
          in: path
          description: UsuarioID que va a verse el historial (mismo que header)
          schema:
            type: integer
          required: true
      responses:
        200:
          description: Muestra historial de pedidos.
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
        405:
          description: No encontro usuario.
    post:
      tags:
        - Usuarios
      summary: Permite agregar dirección
      description: Permite al usuario que agregue una dirección en su agenda de direcciones.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: integer
      requestBody:
        required: true
        content: 
          application/json:
            schema:
              type: object 
              properties:
                idDireccion:
                  type: integer
                  example: 3
                direccion:
                  type: string
                  example: Calle wallaby 42 sydney
                detalle:
                  type: string
                  example: La casa de Nemo
      responses:
        200:
          description: El usuario ha sido suspendido.
        201:
          description: El usuario dejó de estar suspendido.
        404:
          description: Intente nuevamente.
    put:
      tags:
        - Usuarios
      summary: Permite suspender usuario
      description: Permite al usuario administrador logueado, suspender o sacar la suspensión a un usuario.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
        - name: id
          in: path
          description: ID del usuario a quien se va a suspender
          required: true
          schema:
            type: string
      responses:
        200:
          description: El usuario ha sido suspendido.
        201:
          description: El usuario dejó de estar suspendido.
        404:
          description: Intente nuevamente.
    delete:
      tags:
        - Usuarios
      summary: Permite eliminar un usuario.
      description: Permite al usuario administrador eliminar a un usuario.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          schema:
            type: integer
          required: true
        - name: id
          in: path
          description: UsuarioID que va a eliminarse
          schema:
            type: integer
          required: true
      responses:
        200:
          description: Usuario ha sido eliminado.
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
        405:
          description: El usuario no ha sido encontrado.
  /productos:
    get:
      tags:
        - Productos
      summary: Muestra los productos.
      description: Muestra a todos los productos.
      responses:
        200:
          description: Muestra a todos los productos de Delilah.
    post:
      tags:
        - Productos
      summary: Crea nuevos productos.
      description: Permite a los administradores registrados crear nuevos productos
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          schema:
            type: string
          required: true
      requestBody:
        required: true
        content: 
          application/json:
            schema:
              type: object 
              properties:
                producto:
                  type: string
                  example: Hamburguesa de cordero
                precio:
                  type: integer
                  example: 450
      responses:
        200:
          description: El nuevo producto se ha incorporado con éxito.
        401:
          description: Para incorporar el nuevo producto debe completar todos los campos.
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
  /productos/{id}:
    put:
      tags:
        - Productos
      summary: Modifica los productos existentes.
      description: Permite a los administradores registrados modificar elementos de los productos.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
        - name: id
          in: path
          description: ID del producto
          required: true
          schema:
            type: string
      requestBody:
        required: true
        content: 
          application/json:
            schema:
              type: object 
              properties:
                producto:
                  type: string
                  example: Hamburguesa de cordero
                precio:
                  type: integer
                  example: 450
      responses:
        200:
          description: Has modificado el nombre de tu producto.
        201:
          description: Has modificado el precio de tu producto.
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
        405:
          description: No se ha encontrado el producto. Intente nuevamente.
    delete:
      tags:
        - Productos
      summary: Elimina productos existentes.
      description: Permite a los administradores registrados eliminar productos.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
        - name: id
          in: path
          description: ID del producto a eliminar
          required: true
          schema:
            type: string
      responses:
        200:
          description: Producto ha sido eliminado.
        403:
          description: Acceso denegado.

        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
        405:
          description: El producto no ha sido encontrado, intente nuevamente.
  /pedidos:
    get:
      tags:
        - Pedidos
      summary: Muestra todos los pedidos.
      description: Muestra a los administradores registrados la información de todos los pedidos.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
      responses:
        200:
          description: Muestra al administrador toda la información de los pedidos.
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
    post:
      tags:
      - Pedidos
      summary: Inicia el pedido.
      description: Permite a los usuarios registrados iniciar sus pedidos para que posteriormente puedan agregar productos al mismo.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
      responses:
        200:
          description: Iniciaste tu pedido. Ahora vamos a llenar ese carrito!
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
    put:
      tags:
        - Pedidos
      summary: Modifica los estados de los pedidos.
      description: Permite a los administradores registrados modificar el estado de los pedidos. En caso de que el estado ya sea "Entregado", ya no puede modificar su estado.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
      requestBody:
        required: true
        content: 
          application/json:
            schema:
              type: object 
              properties:
                idPedido:
                  type: integer
                  example: 1
                cambioEstado:
                  type: string
                  example: En camino
      responses:
        200:
          description: El pedido ha cambiado su estado a "....".
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
        405:
          description: El pedido no ha sido encontrado.
        406:
          description: Cuando el estado está Entregado no se puede modificar.
  /pedidos/{id}:
    post:
      tags:
        - Pedidos
      summary: Pagar un pedido.
      description: Los usuarios registrados, pueden cerrar sus pedidos. Al hacerlo, pueden optar por la dirección de envío y el método de pago utilizado. Automáticamente el pedido quedará confirmado.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
        - name: id
          in: path
          description: PedidoID que se va a pagar
          required: true
          schema:
            type: string
      requestBody:
        content: 
          application/json:
            schema:
              type: object 
              properties:
                direccion:
                  type: string
                  example: Güemes 150
                  required: false
                idDireccion:
                  type: integer
                  example: 1
                  required: false
                idPago:
                  type: integer
                  example: 2
                  required: true
      responses:
        200:
          description: Su pedido ha sido pagado con éxito. Gracias por pedir a Delilah. Puedes seguir tu pedido para saber donde esta
        402:
          description: Debe seleccionar un método de pago para continuar. Intente nuevamente.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
        405:
          description: El método de pago seleccionado no se encuentra activo en estos momentos. Por favor intente nuevamente con otro....
        406:
          description: Cuando el estado está Entregado no se puede modificar.
    delete:
      tags:
        - Pedidos
      summary: Elimina pedidos existentes.
      description: Permite a los administradores registrados eliminar pedidos.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
        - name: id
          in: path
          description: PedidoID que se va a eliminar
          required: true
          schema:
            type: string
      responses:
        200:
          description: El producto ha sido retirado de tu pedido, probá con otras cosas ricas de Delilah.
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
        405:
          description: No encontramos ese producto, pero no dejes de probar con otras cosas ricas de Delilah.
  /producto/{id}/{in}:
    post:
      tags:
        - Pedidos
      summary: Agrega productos a pedidos existentes.
      description: Permite a los usuarios registrados agregar productos dentro de su pedido, las veces que lo deseen.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
        - name: id
          in: path
          description: PedidoID al que se le agregará el producto.
          required: true
          schema:
            type: string
        - name: in
          in: path
          description: ProductoID que se agregará al pedido.
          required: true
          schema:
            type: string
      responses:
        200:
          description: Un delicioso producto se ha agregado a su pedido. Seguí sumando cosas ricas...
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
        405:
          description: No encontramos ese producto, pero no dejes de probar con otras cosas ricas de Delilah.
    delete:
      tags:
        - Pedidos
      summary: Elimina productos a pedidos existentes.
      description: Permite a los usuarios registrados eliminar productos dentro de su pedido.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
        - name: id
          in: path
          description: PedidoID al que se le quitará el producto.
          required: true
          schema:
            type: string
        - name: in
          in: path
          description: ProductoID que se quitará del pedido.
          required: true
          schema:
            type: string
      responses:
        200:
          description: El producto ha sido retirado de tu pedido, probá con otras cosas ricas de Delilah.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
        405:
          description: No encontramos ese producto, pero no dejes de probar con otras cosas ricas de Delilah.
  /metodosPago:
    get:
      tags:
        - Métodos de pago
      summary: Muestra los métodos de pago.
      description: Muestra a los administradores registrados la información requerida de cada uno de los métodos de pago; ID, nombre, descripción y si se encuentran activos o no.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
      responses:
        200:
          description: Muestra al administrador toda la información de los métodos de pago.
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
    post:
      tags:
        - Métodos de pago
      summary: Crea un nuevo método de pago.
      description: Permite crear a los administradores registrados crear nuevos métodos de pago; ID, nombre y descripción, por defecto el estado será activo.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
      requestBody:
        required: true
        content: 
          application/json:
            schema:
              type: object 
              properties:
                metodoPago:
                  type: string
                  example: Credito
                detalle:
                  type: string
                  example: American Express
      responses:
        200:
          description: El nuevo método de pago se ha incorporado con éxito.
        401:
          description: Para incorporar el nuevo método de pago debe completar todos los campos.
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
  /metodosPago/{id}:
    put:
      tags:
        - Métodos de pago
      summary: Modifica métodos de pago existentes.
      description: Permite a los administradores registrados modificar el estado del método de pago, pasando de activo si está inactivo y viceversa.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
        - name: id
          in: path
          description: ID del método de pago a eliminar
          required: true
          schema:
            type: string
      responses:
        200:
          description: Has modificado el estado de tu método de pago a Activo.
        201:
          description: Has modificado el estado de tu método de pago a Inactivo.
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
        405:
          description: No se ha encontrado tu método de pago. Intente nuevamente.
    delete:
      tags:
        - Métodos de pago
      summary: Elimina métodos de pago existentes.
      description: Permite a los administradores registrados eliminar métodos de pago.
      security:
        - BearerAuth: []
      parameters:
        - name: id
          in: header
          description: UsuarioID de quien va a realizar la operación
          required: true
          schema:
            type: string
        - name: id
          in: path
          description: ID del método de pago a eliminar
          required: true
          schema:
            type: string
      responses:
        200:
          description: El método de pago ha sido eliminado.
        403:
          description: Acceso denegado.
        404:
          description: Debe iniciar sesión para realizar las siguientes acciones.
        405:
          description: El método de pago no ha sido encontrado.
definitions:
  Usuarios:
    type: object
    properties:
      idUsuario:
        type: integer
      soyAdmin:
        type: boolean
      usuario:
        type: string
      nombreApellido:
        type: string
      mail:
        type: mail
      password:
        type: password
      telefono:
        type: integer
      agendaDirecciones:
        type: array
      idProductosHistorial:
        type: array
      logueado:
        type: boolean
      suspendido:
        type: boolean
  Productos:
    type: object
    properties:
      idProducto: 
        type: integer
      producto:
        type: string
      precio:
        type: integer
      activo:
        type: boolean
  Pedidos:
    type: object
    properties:
      idPedido:
        type: integer
      hora:
        type: integer
      idUsuario:
        type: integer
      usuario:
        type: string
      idProductos:
        type: array
      estado:
        type: string
      direccionEnvioPedido:
        type: string
      medioPago:
        type: array
      costoTotal:
        type: integer
      cerrado:
        type: boolean
  MetodosPago:
    type: object
    properties:
      idPago:
        type: integer
      metodoPago:
        type: string
      detalle:
        type: string
      activo:
        type: boolean
components:
  securitySchemes:
    BearerAuth:          
      type: http
      scheme: bearer
      bearerFormat: JWT