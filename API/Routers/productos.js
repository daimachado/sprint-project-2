const express = require('express');
const { mostrarProductos, nuevoProducto, editarProducto, borrarProducto } = require('../Funciones/productos');
const { usuarioAdmin, usuarioSuspendido, usuarioLogueado, autorizacion } = require('../Middleware/delservidor');



function routersProductos(){
    const router = express.Router();
    router.get ('/productos', mostrarProductos);  
    router.post ('/productos', autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, nuevoProducto); 
    router.put ('/productos/:id', autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, editarProducto);
    router.delete ('/productos/:id', autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, borrarProducto); 
    return router;
}

module.exports = {
    routersProductos
}
