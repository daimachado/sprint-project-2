const express = require('express');
const { mostrarPedidos, iniciaPedido, pagarPedido, estadoPedidos, borrarPedidos, agregarProducto, sacarProducto } = require('../Funciones/pedidos');
const { usuarioAdmin, pedidoAbierto, direccionEnvio, seleccionarMedioPago, usuarioSuspendido, autorizacion, usuarioLogueado } = require('../Middleware/delservidor');

function routersPedidos() {
    const router = express.Router();
    router.get('/pedidos',  autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, mostrarPedidos);
    router.post('/pedidos',  autorizacion, usuarioLogueado, usuarioSuspendido, iniciaPedido); //usuarioRegistrado??
    router.post('/pedidos/:id',  autorizacion, usuarioLogueado, usuarioSuspendido, pedidoAbierto, seleccionarMedioPago, direccionEnvio, pagarPedido); //Falta chequear si se guarda en el historial // MIDDLEWARE  seleccionarMedioPago, 
    router.put('/pedidos',  autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, estadoPedidos); // No me anda el IF de si esta entregado no se puede modificar ???
    router.delete('/pedidos/:id',  autorizacion, usuarioLogueado, usuarioSuspendido, usuarioAdmin, borrarPedidos);
    router.post('/producto/:id/:in',  autorizacion, usuarioLogueado, usuarioSuspendido, pedidoAbierto, agregarProducto); //Me borra todos mismo IDS // Falta middleware: usuarioRegistrado, pedidoAbierto
    router.delete('/producto/:id/:in',  autorizacion, usuarioLogueado, usuarioSuspendido, pedidoAbierto, sacarProducto); // NO ME DEJA ELIMINAR ULTIMO PRODUCTO // Me borra todos mismo IDS // Falta middleware: usuarioRegistrado, pedidoAbierto, 
    return router;
}

module.exports = {
    routersPedidos
}