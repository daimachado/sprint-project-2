const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:3000/';


describe('Pruebas unitarias de usuarios', () => {
  it('Registrar un usuario nuevo', () => {
    chai.request(url)
      .post('/users')
      .send({
        usuario: 'TestingUser',
        password: 'laClaveDeTesteo123',
        nombreApellido: 'Testing Name',
        direccionEnvio: 'Testing Address',
        telefono: 123456789,
        mail: 'Testing email'
      })
      .then(function (res) {
        expect(res).to.have.status(201);
      })
      .catch(function (err) {
        throw err;
      });
  });
});